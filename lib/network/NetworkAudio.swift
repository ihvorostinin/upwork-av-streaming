
import AudioToolbox
import CocoaAsyncSocket

class NetworkAudioSessionInfo : NetworkIOSessionInfo {
    let format: AudioFormat.Factory?
    
    init(_ id: IOID, _ format: @escaping AudioFormat.Factory) {
        self.format = format
        super.init(id, data(format))
    }

    override init(_ id: IOID) {
        format = nil
        super.init(id)
    }

    override init(_ id: IOID, _ format: NSData.Factory?) {
        if format != nil {
            self.format = audioFormat(format!)
        }
        else {
            self.format = nil
        }
        super.init(id, format)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// NetworkAudioSerializer
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class NetworkAudioSerializer : AudioOutputProtocol, IOQoSProtocol {
 
    private let output: IODataProtocol?
    private var qid: String = ""

    init(_ output: IODataProtocol?) {
        self.output = output
    }
    
    func change(_ toQID: String, _ diff: Int) {
        self.qid = toQID
    }

    func process(_ packet: AudioData) {
    
        let s = PacketSerializer()
        var t = AudioTime(packet.time)
        
        s.push(&t, MemoryLayout<AudioTime>.size)
        s.push(string: qid)
        s.push(packet.data.bytes, packet.data.length)
        s.push(array: packet.desc)
        
        output?.process(s.data)
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// NetworkAudioDeserializer
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class NetworkAudioDeserializer : IODataProtocol {
    
    private let output: AudioOutputProtocol?
    
    init(_ output: AudioOutputProtocol) {
        self.output = output
    }
    
    func process(_ data: NSData) {
        
        let deserializer = PacketDeserializer(data)
        var time = AudioTime()
        var data: NSData?
        var desc: [AudioStreamPacketDescription]?
        
        deserializer.pop(&time)
        _ = deserializer.popSkip()
        deserializer.pop(data: &data)
        deserializer.pop(array: &desc)

        output?.process(AudioData(time.ToAudioTimeStamp(), data!, desc))
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// NetworkAudioUDPOutput
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class NetworkAudioUDPOutput :
    NSObject,
    AudioOutputProtocol,
    IOSessionProtocol,
    GCDAsyncUdpSocketDelegate {
    
    private let next: AudioOutputProtocol
    private var socket: GCDAsyncUdpSocket?
    
    private let port = 20002
    private let host = "192.168.8.102"
    
    private var startTime = 0
    private var sequenceNum = 0
    
    init(_ next: AudioOutputProtocol) {
        self.next = next
    }
    
    func start() throws {
        socket = GCDAsyncUdpSocket(delegate: self, delegateQueue: DispatchQueue.main)
    }
    
    func stop() {
        
    }
    
    func process(_ data: AudioData) {
        next.process(data)

        // fill the header array of byte with RTP header fields
        
        var header = rtp_header()

        header.v = 2
        header.p = 0
        header.x = 0
        header.cc = 0
        header.m = 0
        header.pt = 97
        header.seq = UInt32(sequenceNum)
        header.ts = UInt32(startTime)
        header.ssrc = UInt32(port)
        
        // send packet
        
        for i in data.desc! {
            let packet = NSMutableData(bytes: &header, length: 12)
            packet.append(NSData(bytes: data.data.bytes.advanced(by: Int(i.mStartOffset)),
                                 length: Int(i.mDataByteSize)) as Data)

            socket?.send(packet as Data, toHost: host, port: UInt16(port), withTimeout: -1, tag: 0)

            sequenceNum += 1
            startTime += 1
//            _ = data.desc?.map({ startTime += Int($0.mVariableFramesInPacket) })
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // GCDAsyncUdpSocketDelegate
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
//    - (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data
//    fromAddress:(NSData *)address
//    withFilterContext:(nullable id)filterContext;

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Audio format
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension AudioFormat {
    
    func toNetwork() throws -> NSData {
        return try JSONSerialization.data(withJSONObject: format.data,
                                          options: JSONSerialization.defaultWritingOptions) as NSData
    }
    
    static func fromNetwork(_ data: NSData) throws -> AudioFormat {
        let json = try JSONSerialization.jsonObject(with: data as Data,
                                                    options: JSONSerialization.ReadingOptions()) as! [String: Any]
        return AudioFormat(IOFormat(json))
    }
}

func data(_ src: @escaping AudioFormat.Factory) -> NSData.Factory {
    return { return try src().toNetwork() }
}

func audioFormat(_ src: @escaping NSData.Factory) -> AudioFormat.Factory {
    return { return try AudioFormat.fromNetwork(src()) }
}


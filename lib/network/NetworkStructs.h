//
//  NetworkStructs.h
//  AVStreaming
//
//  Created by Ivan Khvorostinin on 05/07/2017.
//  Copyright © 2017 ikhvorostinin. All rights reserved.
//

#pragma once

struct rtp_header {
    unsigned int v:2; /* protocol version */
    unsigned int p:1; /* padding flag */
    unsigned int x:1; /* header extension flag */
    unsigned int cc:4; /* CSRC count */
    unsigned int m:1; /* marker bit */
    unsigned int pt:7; /* payload type */
    unsigned int seq:16; /* sequence number */
    unsigned int ts:32; /* timestamp */
    unsigned int ssrc:32; /* synchronization source */
};
